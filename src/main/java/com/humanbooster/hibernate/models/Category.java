package com.humanbooster.hibernate.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Basic
    private String name;



    @ManyToOne
    @JoinColumn(name = "parent_category_id")
    private Category parentId;

    @OneToMany(mappedBy = "parentId")
    List<Category> childrens;

    public List<Category> getChildrens(){
        return this.childrens;
    }

    public void addChild(Category category){
        this.childrens.add(category);
    }

    public void removeChild(Category category){
        this.childrens.remove(category);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getParentId() {
        return parentId;
    }

    public void setParentId(Category parentId) {
        this.parentId = parentId;
    }
}
