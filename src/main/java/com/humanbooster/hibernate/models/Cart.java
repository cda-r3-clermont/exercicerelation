package com.humanbooster.hibernate.models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createdAt;

    @Column(name = "validated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar validatedAt;

    @OneToMany(mappedBy = "cart")
    private List<User> users;

    public Cart() {
        this.users = new ArrayList<User>();
    }

    public List<User> getUsers(){
        return this.users;
    }

    public void addUser(User user){
        users.add(user);
    }

    public void remove(User user){
        this.users.remove(user);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    public Calendar getValidatedAt() {
        return validatedAt;
    }

    public void setValidatedAt(Calendar validatedAt) {
        this.validatedAt = validatedAt;
    }
}
