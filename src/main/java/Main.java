import com.humanbooster.utils.HibernateUtil;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class Main {
    public static void main(String[] args)  {
        // Ici, nous réccupérons une session factory
        SessionFactory sessionHibernate = HibernateUtil.getSessionFactory();

        sessionHibernate.close();
    }
}




